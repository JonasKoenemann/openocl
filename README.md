# Open Optimal Control Library

OpenOCL is hosted on github, please go to the [github repository](https://github.com/OpenOCL/OpenOCL).

Visit the main website [openocl.org](https://openocl.org) to [download](https://openocl.org/get-started/) the toolbox, go through the [tutorial](https://openocl.org/tutorial/), and have a look [API Docs](https://openocl.org/api-docs/) and the [examples](https://github.com/JonasKoenemann/optimal-control/tree/master/Examples).

Jonas Koenemann  
Jonas@openocl.org


